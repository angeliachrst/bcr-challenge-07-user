import styles from "../styles/CtaBanner.module.css";
import Link from "next/link";

const CtaBanner = () => {
  return (
    <>
      <div className={styles.ctaBanner}>
        <div className={styles.ctaText}>
          <h1 className={styles.ctaText}>Sewa Mobil di (Lokasimu) Sekarang</h1>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod <br /> tempor incididunt ut labore et dolore magna aliqua.
          </p>
          <button className={`btn btn-success ${styles.btnCta}`}>
            <Link href="/carimobil">Mulai Sewa Mobil</Link>
          </button>
        </div>
      </div>
    </>
  );
};

export default CtaBanner;
