import styles from '../styles/Faq.module.css';
const Faq = () => {
  return (
    <>
      <div id="faq" className={styles.faq}>
        <div className={styles.faqRow}>
          <h2>Frequently Asked Question</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
        </div>
        <div className={styles.faqRow}>
          <ul className={`list-group ${styles.faqUl}`}>
            <li className={`list-group-item d-flex justify-content-between align-items-center ${styles.liFaq}`}>
              Apa saja syarat yang dibutuhkan?
              <span>
                <img src="/img/dropdown.svg" alt="" />
              </span>
            </li>
            <li className={`list-group-item d-flex justify-content-between align-items-center ${styles.liFaq}`}>
              Berapa hari minimal sewa mobil lepas kunci?
              <span>
                <img src="/img/dropdown.svg" alt="" />
              </span>
            </li>
            <li className={`list-group-item d-flex justify-content-between align-items-center ${styles.liFaq}`}>
              Berapa hari sebelumnya sebaiknya booking sewa mobil?
              <span>
                <img src="/img/dropdown.svg" alt="" />
              </span>
            </li>
            <li className={`list-group-item d-flex justify-content-between align-items-center ${styles.liFaq}`}>
              Apakah Ada biaya antar-jemput?
              <span>
                <img src="/img/dropdown.svg" alt="" />
              </span>
            </li>
            <li className={`list-group-item d-flex justify-content-between align-items-center ${styles.liFaq}`}>
              Bagaimana jika terjadi kecelakaan
              <span>
                <img src="/img/dropdown.svg" alt="" />
              </span>
            </li>
          </ul>
        </div>
      </div>
    </>
  );
};

export default Faq;
