import styles from "../styles/Header.module.css";
import Link from 'next/link';

const Header = () => {
  return (
    <>
      <header className={styles.header}>
        <nav className={`navbar sticky-top ${styles.paddingNav}`}>
          <div className="container">
            <div className={styles.logo}></div>
            <div>
              <ul className="navbar-nav flex-row">
                <li className="nav-item">
                  <a className={styles.navLink} href="#ourServices" style={{ marginRight: "12px" }}>
                    Our Services
                  </a>
                </li>
                <li className="nav-item">
                  <a className={styles.navLink} href="#whyUs" style={{ marginRight: "12px" }}>
                    Why Us
                  </a>
                </li>
                <li className="nav-item">
                  <a className={styles.navLink} href="#testimonial" style={{ marginRight: "12px" }}>
                    Testimonial
                  </a>
                </li>
                <li className="nav-item">
                  <a className={styles.navLink} href="#faq" style={{ marginRight: "12px" }}>
                    FAQ
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    style={{
                      background: "#5CB85F",
                      borderRadius: "2px",
                      color: "white",
                      padding: "8px 12px",
                      marginLeft: "16px",
                      fontWeight: "700"
                    }}
                    href="#register"
                  >
                    Register
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <div className="container">
          <section className={`row ${styles.hero}`}>
            <div className="col-md-6 col-sm-12 align-self-center">
              <h1>
                Sewa & Rental Mobil Terbaik di <br /> kawasan (Lokasimu)
              </h1>
              <p>
                Selamat datang di Binar Car Rental. Kami menyediakan mobil
                kualitas terbaik dengan harga terjangkau. Selalu siap melayani
                kebutuhanmu untuk sewa mobil selama 24 jam.
              </p>
              <button className={`${styles.btnHero} btn btn-succes`}>
                <Link href="/carimobil">Mulai Sewa Mobil</Link>
              </button>
            </div>
            <div className="col-md-6 col-sm-12">
              <img
                src="/img/img_car.png"
                className={styles.imgHero}
                alt="hero"
              />
            </div>
          </section>
        </div>
      </header>
    </>
  );
};

export default Header;
