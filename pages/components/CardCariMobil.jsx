import styles from "../../styles/CardCariMobil.module.css";
import Link from "next/link";

const CardCariMobil = () => {
  return (
    <>
      <div
        className={`${styles.cariMobil} position-absolute start-50 translate-middle bg-light`}
      >
        <div className={styles.frameCari}>
          <div className={styles.frameInput}>
            <label>Tipe Driver</label>
            <select className={styles.inputStyle}>
              <option value="" disabled selected>
                Pilih Tipe Driver
              </option>
              <option value="1">Dengan Sopir</option>
              <option value="2">Tanpa Sopir (Lepas Kunci)</option>
            </select>
          </div>
          <div className={styles.frameInput}>
            <label>Tanggal</label>
            <input
              type="date"
              className={styles.inputStyle}
              placeholder="Pilih Tanggal"
            ></input>
          </div>
          <div className={styles.frameInput}>
            <label>Waktu Jemput/Ambil</label>
            <input
              type="time"
              className={styles.inputStyle}
              placeholder="Pilih Waktu"
            ></input>
          </div>
          <div className={styles.frameInput}>
            <label>Jumlah Penumpang (optional)</label>
            <input
              className={styles.inputStyle}
              placeholder="Jumlah Penumpang"
            ></input>
          </div>
          <div className={styles.frameInput}>
            <button className={styles.buttonCari}>
              <Link href="/hasilpencarian">Cari Mobil</Link>
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default CardCariMobil;
