import styles from '../../styles/Invoice.module.css';
import { Chart } from 'chart.js/auto';
import { Bar } from 'react-chartjs-2';

const Invoice = () => {
    const dataChart = {
        labels: ['Innova', 'Toyota', 'Karimun', 'Ayla'],
        datasets: [
            {
                label: 'Disewakan',
                backgroundColor: 'royalblue',
                data: [50,12,26,63]
            },
        ]
    }

    return (
        <>
        <div className="container">
            <div className={`${styles.invoice} position-absolute start-50`}>
                <img className={styles.imgSuccess} src="/img/success.svg" alt="success" />
                <h1 className={styles.textInvoice} style={{fontWeight: 700}}>Pembayaran Berhasil!</h1>
                <p className={styles.textInvoice}>
                Tunjukkan invoice ini ke petugas BCR di titik temu.
                </p>
                <div style={{ width: 400, marginTop: "16px" }}>
                    <Bar data={dataChart} redraw={true} />
                </div>
            </div>
        </div>
        </>
    )
}

export default Invoice;