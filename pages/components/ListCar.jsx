import styles from "../../styles/ListCar.module.css";
import Link from "next/link";
// import { useSelector } from "react-redux";

const ListCar = () => {
  const dataCar = [
    {
      id: "1",
      name: "Innova",
      price: "300.000",
      image: "/img/car.png",
    },
  ];

  return (
    <>
      <div className="container">
        <div className={styles.listCar}>
          {dataCar?.map((car) => (
            <div key={car.id} className={styles.cardMobil}>
              <div className={styles.frameCard}>
                <img className={styles.cardImage} src={car.image} alt="mobil" />
                <div className={styles.textCard}>
                  <p>{car.name}</p>
                  <h1>Rp {car.price} / hari</h1>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua.
                  </p>
                </div>
                <div className={styles.iconCard}>
                  <img src="/img/users.svg" alt="jumlah orang" />
                  <p>4 orang</p>
                </div>
                <div className={styles.iconCard}>
                  <img src="/img/settings.svg" alt="tipe mobil" />
                  <p>Manual</p>
                </div>
                <div className={styles.iconCard}>
                  <img src="/img/calendar.svg" alt="tahun" />
                  <p>Tahun 2020</p>
                </div>
                <div className={styles.buttonCar}>
                  <Link href={`/hasilpencarian/1`}>Pilih Mobil</Link>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

export default ListCar;
