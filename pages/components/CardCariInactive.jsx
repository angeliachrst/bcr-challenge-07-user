import styles from"../../styles/CardCariInactive.module.css";
const CardCariInactive = () => {
  return (
    <>
      <div className={`${styles.cariInactive} position-absolute start-50 translate-middle bg-light`}>
      <h1>Pencarianmu</h1>
        <div className={styles.frameCari}>
          <div className={styles.frameInput}>
            <label>Tipe Driver</label>
            <input type="text" className={styles.inputStyle} disabled></input>
          </div>
          <div className={styles.frameInput}>
            <label>Tanggal</label>
            <input type="date" className={styles.inputStyle} disabled></input>
          </div>
          <div className={styles.frameInput}>
            <label>Waktu Jemput/Ambil</label>
            <input type="time" className={styles.inputStyle} disabled></input>
          </div>
          <div className={styles.frameInput}>
            <label>Jumlah Penumpang (optional)</label>
            <input className={styles.inputStyle} disabled></input>
          </div>
          <div className={styles.frameInput}></div>
        </div>
      </div>
    </>
  );
};

export default CardCariInactive;
