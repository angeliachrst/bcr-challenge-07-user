import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Invoice from '../components/Invoice';
import Head from 'next/head';
import styles from '../../styles/Header.module.css';
import styles1 from '../../styles/Footer.module.css';
import { useEffect } from 'react';

const Pembayaran = () => {
    useEffect(() => {
        document.getElementsByClassName(styles.btnHero)[0].style.display = "none";
        document.getElementsByClassName(styles.hero)[0].style.display = "none";
        document.getElementsByClassName(styles.header)[0].style.height = "164px";
        document.getElementsByClassName(styles1.footer)[0].style.marginTop = "450px";
    }, []);
    return (
        <>
            <Head>
                <title>Pembayaran</title>
            </Head>
            <Header />
            <Invoice />
            <Footer />
        </>
    )
}

export default Pembayaran;