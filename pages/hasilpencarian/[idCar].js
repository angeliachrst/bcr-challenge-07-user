import { useRouter } from 'next/router';
import { useEffect } from 'react';
import Link from 'next/link';
import Head from 'next/head';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import CardCariInactive from '../components/CardCariInactive';
import styles from '../../styles/DetailCar.module.css';
import styles1 from '../../styles/Header.module.css';
import styles2 from '../../styles/Footer.module.css';

const DetailCar = () => {
    const router = useRouter();
    const { idCar } = router.query;

    useEffect(() => {
        document.getElementsByClassName(styles1.btnHero)[0].style.display = "none";
        document.getElementsByClassName(styles1.hero)[0].style.display = "none";
        document.getElementsByClassName(styles1.header)[0].style.height = "266px";
        document.getElementsByClassName(styles2.footer)[0].style.marginTop = "890px";
    }, []);

    return (
        <>
            <Head>
                <title>Hasil Pencarian</title>
            </Head>
            <Header />
            <CardCariInactive />
            <div className={`${styles.detailCar} position-absolute start-50 translate-middle`}>
                <div className={styles.cardDetail}>
                    <h1>Tentang Paket</h1>
                    <p>Include</p>
                    <div className={styles.textDetail1}>
                        <ul>
                            <li>
                                Apa saja yang termasuk dalam paket misal durasi max 12 jam
                            </li>
                            <li>Sudah termasuk bensin selama 12 jam</li>
                            <li>Sudah termasuk Tiket Wisata</li>
                            <li>Sudah termasuk pajak</li>
                        </ul>
                    </div>
                    <p>Exclude</p>
                    <div className={styles.textDetail2}>
                        <ul>
                            <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                            <li>
                                Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp
                                20.000/jam
                            </li>
                            <li>Tidak termasuk akomodasi penginapan</li>
                        </ul>
                    </div>
                    <h1>Refund, Reschedule, Overtime</h1>
                    <div className={styles.textDetail3}>
                        <ul>
                            <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                            <li>
                                Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp
                                20.000/jam
                            </li>
                            <li>Tidak termasuk akomodasi penginapan</li>
                            <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                            <li>
                                Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp
                                20.000/jam
                            </li>
                            <li>Tidak termasuk akomodasi penginapan</li>
                            <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                            <li>
                                Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp
                                20.000/jam
                            </li>
                            <li>Tidak termasuk akomodasi penginapan</li>
                        </ul>
                    </div>
                </div>
                <div className={styles.cardDetail}>
                    <div className={styles.cardImage}>
                        <img src="/img/car.png" alt="car" />
                    </div>
                    <div className={styles.cardTitle}>
                        <h1>Innova</h1>
                        <div className={styles.cardIcon}>
                            <div className={styles.iconTitle}>
                                <img src="/img/users.svg" alt="jumlah orang" />
                                <p>4 orang</p>
                            </div>
                            <div className={styles.iconTitle}>
                                <img src="/img/settings.svg" alt="tipe mobil" />
                                <p>Manual</p>
                            </div>
                            <div className={styles.iconTitle}>
                                <img src="/img/calendar.svg" alt="calendar" />
                                <p>Tahun 2020</p>
                            </div>
                        </div>
                        <div className={styles.cardText}>
                            <p>Total</p>
                            <h1>Rp 300.000 / hari</h1>
                        </div>
                        <Link href="/pembayaran">
                            <button className={styles.buttonCard}>Lanjutkan Pembayaran</button>
                        </Link>
                    </div>
                </div>
            </div>
            <Link href="/pembayaran">
                <div className={`${styles.buttonPosition} position-absolute start-50 translate-middle`}>
                    <button className={styles.buttonCard}>Lanjutkan Pembayaran</button>
                </div>
            </Link>
            <Footer />
        </>
    )
}

export default DetailCar;