import Header from '../../components/Header';
import Footer from '../../components/Footer';
import CardHasilPencarian from '../components/CardHasilPencarian';
import ListCar from '../components/ListCar';
import Head from 'next/head';
import styles from '../../styles/Header.module.css';
import styles1 from '../../styles/Footer.module.css';
import { useEffect } from 'react';

const HasilPencarian = () => {
    useEffect(() => {
        document.getElementsByClassName(styles.btnHero)[0].style.display = "none";
        document.getElementsByClassName(styles.hero)[0].style.display = "none";
        document.getElementsByClassName(styles.header)[0].style.height = "266px";
        document.getElementsByClassName(styles1.footer)[0].style.marginTop = "100px";
    }, []);

    return (
        <>
            <Head>
                <title>Hasil Pencarian</title>
            </Head>
            <Header />
            <CardHasilPencarian />
            <ListCar />
            <Footer />
        </>
    )
}

export default HasilPencarian;