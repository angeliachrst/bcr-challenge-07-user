import Header from '../../components/Header';
import Footer from '../../components/Footer';
import CardCariMobil from '../components/CardCariMobil';
import styles from '../../styles/Header.module.css';
import { useEffect } from 'react';
import Head from 'next/head';

const CariMobil = () => {
    useEffect(() => {
        document.getElementsByClassName(styles.btnHero)[0].style.display = "none";
    }, []);

    return (
        <>
        <Head>
            <title>Cari Mobil</title>
        </Head>
        <Header />
        <CardCariMobil />
        <Footer />
        </>
    )
}

export default CariMobil;